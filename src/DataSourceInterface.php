<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

/**
 *
 * @author <ermanno.astolfi@spinit.it>
 */
interface DataSourceInterface {
    function connect($conString);
    
    function insert($resource, $data);
    function update($resource, $data, $key);
    function delete($resource, $key);
    
    function query($cmd);
    function exec($cmd);
    
    function check($resourceName);
    function align($resourceStruct);
    function drop($resourceName);
    function truncate($resourceName);
    
    function getPkeyDefault();
    function getCounter();
    function getConnectionString();
}
