<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use Spinit\Lib\Model\Model as ModelMain;

/**
 * Description of Model
 *
 * @author ermanno
 */
abstract class Model extends ModelMain {
    
    private $DB;
    
    public function __construct(DataSource $DB) {
        parent::_construct();
        $this->DB = $DB;
    }
    
    public function getDataSource()
    {
        return $this->DB;
    }
}
