<?php
namespace Spinit\Lib\DataSource\PDO\Strategy;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Spinit\Lib\DataSource\Type\StrategyInterface;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of DaImplementare
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DaImplementare implements StrategyInterface
{
    
    public function exec($observer = null)
    {
        throw new NotFoundException('Comando PDO da implementare');
    }

}
