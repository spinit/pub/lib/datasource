<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Strategy;

use Spinit\Lib\DataSource\Type\StrategyInterface;
use Spinit\Util;

/**
 * Description of TableCreate
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CommandDelete implements StrategyInterface
{
    private $info;
    private $resource;
    private $pkey;
    private $ds;
    
    public function __construct($ds, $resource, $pkey)
    {
        $this->resource = $resource;
        $this->ds = $ds;
        $this->info = $ds->check($resource);
        if (!is_array($pkey)) {
            $pkey = ['id' => $pkey];
        }
        $this->pkey = $pkey;
        //$this->first = $ds->convert($this->info['fields'], $ds->select($this->info['name'], $pkey, '', 1)->first());
    }
    public function exec($observer = null)
    {
        $args = func_get_args();
        array_shift($args);
        $forceDelete = array_shift($args);
        $data = [];
        $where = [];
        foreach($this->pkey as $name => $value) {
            $type = $this->ds->getLib()->getDataType(Util\arrayGet($this->info, ['fields', $name, 'type']), []);
            list($fieldName, $fieldValue, $dataName, $dataValue) = $type->serializeData($name, $value, 'whr_');
            if ($fieldValue === null) {
                $where[] = $fieldName.' IS NULL';
            } else if (is_array($fieldValue)) {
                $where[] = $fieldName.' in ('.implode(', ', $fieldValue).')';
            } else {
                $where[] = $fieldName.' = '.$fieldValue;
            }
            if ($dataName) {
                if (is_array($dataName)) {
                    foreach($dataName as $k=>$v) {
                        $data[$v] = $dataValue[$k];
                    }
                } else {
                    $data[$dataName] = $dataValue;
                }
            }
        }
        $kk = array_keys(Util\arrayGet($this->info, 'fields'));
        if (count($where) == 0) {
            return;
        }
        if (in_array('dat_del__', $kk) and !$forceDelete) {
            $fields = ["dat_del__ = now()"];
            if (in_array('usr_del__', $kk)) {
                if ($this->ds->getParam('userTrace')) {
                    $fields[] = "usr_del__ = unhex('".$this->ds->getParam('userTrace')."')";
                }
            }
            $sql = "UPDATE {$this->info['name']} SET ".implode(', ', $fields)." WHERE ".implode(' AND ', $where);
            $event = 'delete-soft';
        } else {
            $sql = "DELETE FROM {$this->info['name']} WHERE ".implode(' AND ', $where);
            $event = 'delete';
        }
        $ret = $this->ds->exec($sql, $data);
        $observer && (is_callable($observer) ? call_user_func_array($observer, [$event]): $observer->trigger($event));
        return $ret;
    }

}
