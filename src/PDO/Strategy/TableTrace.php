<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Strategy;

use Spinit\Lib\DataSource\Type\StrategyInterface;
use Spinit\Util;

/**
 * Description of TableCreate
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class TableTrace implements StrategyInterface
{
    private $info;
    private $ds;
    private $cmd;
    private $traceFields = [
        'usr_ins__' => 'uuid',
        'dat_ins__' => 'datetime',
        'usr_upd__' => 'uuid',
        'dat_upd__' => 'datetime',
        'usr_del__' => 'uuid',
        'dat_del__' => 'datetime',
    ];
    public function __construct($ds, $info, StrategyInterface $cmd = null)
    {
        $this->info = $info;
        $this->ds = $ds;
        $this->cmd  = $cmd;
    }
    public function exec($observer = null)
    {
        $this->cmd && $this->cmd->exec();
        switch(Util\arrayGet($this->info, 'trace')) {
            case '':
                break;
            default :
                $this->checkTraceRecord();
        }
        return;
    }
    
    private function checkTraceRecord()
    {
        $info = $this->ds->check(Util\arrayGetAssert($this->info, 'name'), 1);
        $tt = array_keys(Util\arrayGet($info, 'fields', []));
        try {
            foreach($this->traceFields as $name => $type) {
                if (!in_array($name, $tt)) {
                    $cmd = 'ALTER TABLE '.$info['name'].' ADD '.$this->ds->getLib()->makeField($name, ['type'=>$type]);
                    $this->ds->exec($cmd);
                }
            }
        }
        catch (\Exception $e) {
            //debug($tt, $info, $e->getMessage());
        }
    }
}
