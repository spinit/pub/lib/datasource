<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Strategy;

use Spinit\Lib\DataSource\Type\StrategyInterface;
use Spinit\Util;

/**
 * Description of TableCreate
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class TableUpgrade implements StrategyInterface
{
    private $info;
    private $check;
    private $ds;
    
    public function __construct($ds, $info, $check)
    {
        $this->info = $info;
        $this->check = $check;
        $this->ds = $ds;
    }
    public function exec($observer = null)
    {
        $modified = false;
        foreach(Util\arrayGet($this->info, 'fields', []) as $name => $conf) {
            if (!array_key_exists($name, $this->check['fields'])) {
                $cmd = $this->ds->getLib()->addColumn($this->info['name'], $name, $conf);
                $this->ds->exec($cmd);
                $modified = true;
            } else {
                if (Util\arrayGet($conf, 'size') and $conf['size'] != $this->check['fields'][$name]['size']) {
                    //$this->ds->getLib()->upgradeColumn($info['name'], $name, $conf);
                }
            }
        }
        return $modified;
    }

}
