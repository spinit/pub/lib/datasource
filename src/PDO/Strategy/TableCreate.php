<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Strategy;

use Spinit\Lib\DataSource\Type\StrategyInterface;
use Spinit\Util;

/**
 * Description of TableCreate
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class TableCreate implements StrategyInterface
{
    private $info;
    private $ds;
    
    public function __construct($ds, $info)
    {
        $this->info = $info;
        $this->ds = $ds;
    }
    public function exec($observer = null)
    {
        $this->execCommand();
        if (Util\arrayGet($this->info, 'name')) {
            $this->createTable();
        }
        return true;
    }
    private function execCommand() {
        if (!count(Util\arrayGet($this->info, 'exec') ?: [])) {
            return;
        }
        $this->ds->bindExec('flush', function () {
            $ret = null;
            foreach($this->info['exec'] as $query) {
                $sql = $this->ds->getLib()->getQuery($query);
                if (!trim($sql)) continue;
                $ret = $this->ds->exec($sql);
            }
            $ret;
        });
    }
    private function createTable($observer = null)
    {
        $head = 'CREATE TABLE '.Util\arrayGetAssert($this->info, 'name').PHP_EOL;
        $fields = [];
        $pkey = [];
        foreach(Util\arrayGet($this->info, 'fields', []) as $name => $conf) {
            if (Util\arrayGet($conf, 'ispkey') or Util\arrayGet($conf, 'pkey')) {
                $pkey[] = $name;
            }
            $fields[$name] = $this->ds->getLib()->makeField($name, $conf);
        }
        if (count($pkey)) {
            $fields[] = $this->ds->getLib()->makePkey($pkey);
        }
        $cmd = $head."(".implode(",\n", $fields).")";
        $ret = $this->ds->exec($cmd);
        $event = 'create';
        $observer && (is_callable($observer) ? call_user_func_array($observer, [$event]): $observer->trigger($event));
        return $ret;
    }
}
