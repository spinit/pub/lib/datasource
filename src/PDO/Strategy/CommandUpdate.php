<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Strategy;

use Spinit\Lib\DataSource\Type\StrategyInterface;
use Spinit\Util;

/**
 * Description of TableCreate
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CommandUpdate implements StrategyInterface
{
    private $info;
    private $resource;
    private $data;
    private $pkey;
    private $ds;
    
    public function __construct($ds, $resource, $data, $pkey)
    {
        $this->resource = $resource;
        $this->data = $data;
        $this->ds = $ds;
        $this->info = $ds->check($resource);
        $this->pkey = is_array($pkey) ? $pkey : ['id'=>$pkey];
        $this->first = $ds->convert($this->info['fields'], $ds->select($this->info['name'], $pkey)->first());
    }
    public function exec($observer = null)
    {
        $data = [];
        $fields = [];
        foreach($this->data as $name => $value) {
            if (Util\arrayGet($this->first, $name) == $value) {
                continue;
            }
            $type = $this->ds->getLib()->getDataType(Util\arrayGet($this->info, ['fields', $name, 'type']), []);
            list($fieldName, $fieldValue, $dataName, $dataValue) = $type->serializeData($name, $value, 'set_');
            // mettendo più spazi davanti a NULL ... non viene trasformato in IS NULL
            $fields[] = $fieldName.($fieldValue === null ? ' =  NULL' : ' = '.$fieldValue);
            if ($dataName) {
                $data[$dataName] = $dataValue;
            }
        }
        if (!count($fields)) {
            return '';
        }
        $kk = array_keys(Util\arrayGet($this->info, 'fields'));
        if (in_array('usr_upd__', $kk)) {
            if ($this->ds->getParam('userTrace')) {
                $fields[] = "usr_upd__ = unhex('".$this->ds->getParam('userTrace')."')";
            }
        }
        if (in_array('dat_upd__', $kk)) {
            $fields[] = "dat_upd__ = '".date('Y-m-d H:i:s')."'";
        }
        $where = [];
        foreach($this->pkey as $name => $value) {
            $type = $this->ds->getLib()->getDataType(Util\arrayGet($this->info, ['fields', $name, 'type']), []);
            list($fieldName, $fieldValue, $dataName, $dataValue) = $type->serializeData($name, $value, 'whr_');
            if ($fieldValue === null) {
                $where[] = $fieldName.' IS NULL';
            } else {
                $where[] = $fieldName.' = '.$fieldValue;
            }
            if ($dataName) {
                $data[$dataName] = $dataValue;
            }
        }
        $sql = "UPDATE {$this->info['name']} SET ".implode(', ', $fields)." WHERE ".implode(' AND ', $where);
        $ret = $this->ds->exec($sql, $data);
        $event = 'update';
        $observer && (is_callable($observer) ? call_user_func_array($observer, [$event]): $observer->trigger($event));
        return $ret;
    }

}
