<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Strategy;

use Spinit\Lib\DataSource\Type\StrategyInterface;
use Spinit\Util;
use Webmozart\Assert\Assert;
/**
 * Description of TableCreate
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CommandInsert implements StrategyInterface
{
    private $info;
    private $resource;
    private $data;
    private $ds;
    
    public function __construct($ds, $resource, $data)
    {
        $this->resource = $resource;
        Assert::isArray($data, 'Record da inserire non impostato');
        $this->data = $data;
        $this->ds = $ds;
        $this->info = $ds->check($resource);
        Assert::notnull($this->info['name'], 'Insert : Nome Tabella non impostato su '.$resource);
    }
    public function exec($observer = NULL)
    {
        $data = [];
        foreach($this->data as $name => $value) {
            $type = $this->ds->getLib()->getDataType(Util\arrayGet($this->info, ['fields', $name, 'type']), []);
            
            list($fieldName, $fieldValue, $dataName, $dataValue) = $type->serializeData($name, $value);
            $fields[] = $fieldName;
            $values[] = ($fieldValue===null ? 'NULL' : $fieldValue);
            if ($dataName) {
                $data[$dataName] = $dataValue;
            }
        }
        $kk = array_keys(Util\arrayGet($this->info, 'fields'));
        if (in_array('usr_ins__', $kk)) {
            if ($this->ds->getParam('userTrace')) {
                $fields[] = 'usr_ins__';
                $values[] = "unhex('".$this->ds->getParam('userTrace')."')";
            }
        }
        if (in_array('dat_ins__', $kk)) {
            $fields[] = 'dat_ins__';
            $values[] = "'".date('Y-m-d H:i:s')."'";
        }
        $sql = "INSERT INTO {$this->info['name']} (".implode(', ', $fields).") VALUES (".implode(', ', $values).")";
        $ret = $this->ds->exec($sql, $data);
        $event = 'insert';
        $observer && (is_callable($observer) ? call_user_func_array($observer, [$event]): $observer->trigger($event));
        return $ret;
}

}
