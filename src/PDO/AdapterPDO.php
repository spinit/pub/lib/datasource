<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO;

use Spinit\Lib\DataSource\AdapterInterface;
use Spinit\Lib\DataSource\PDO\DataLib;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;
use Spinit\UUIDO;
use Webmozart\Assert\Assert;

/**
 * Description of DataSource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class AdapterPDO implements AdapterInterface
{
    private $pdo;
    private $info;
    private $lib;
    private $commandLast;
    private $pager;
    private $cacheResource = [];
    static $UUID_SIZE = 16;
    
    use Util\ParamTrait, Util\TriggerTrait;
    
    public function __construct() {
        $this->info = func_get_args();
        $this->lib = $this->makeLib();
        $this->pdo = $this->newPdo();
    }
    public function reconnect() {
        if ($this->pdo) $this->pdo = null;
        $this->pdo = $this->newPdo();
    }
    abstract protected function makeLib();
    abstract protected function newPDO();
    
    public function getLib() {
        return $this->lib;
    }
    public function normalize() {
        $args = func_get_args();
        return call_user_func_array([$this->getLib(), 'normalize'], $args);
    }
    public function getInfo() {
        $args = func_get_args();
        if (count($args)) {
            return Util\arrayGet($this->info, $args[0]);
        }
        return $this->info;
    }
    public function getPkeyDefault() {
        return 'id';
    }
    public function check($info)
    {
        $args = func_get_args();
        array_shift($args);
        $nocache = array_shift($args);
        if (is_array($info)) {
            $resource = Util\arrayGetAssert($info, 'name');
        } else {
            $resource = $info;
        }
        if (!$resource) {
            return false;
        }
        try {
            if (!array_key_exists($resource, $this->cacheResource) or !$this->cacheResource[$resource] or $nocache) {
                $this->cacheResource[$resource] = $this->getLib()->getDataStruct($resource);
            }
            return $this->cacheResource[$resource];
        } catch (NotFoundException $e) {
            return false;
        }
    }
    
    public function getManager($strict = false)
    {
        return $this->pdo;
    }
    public function setCommandLast()
    {
        $this->commandLast = func_get_args();
        $this->commandLast[0] = "\n\n".$this->commandLast[0]."\n\n";
        return $this;
    }
    public function getcommandLast()
    {
        return $this->commandLast;
    }
    public function exec($cmd)
    {
        $args = func_get_args();
        list($sql, $param) = call_user_func_array([$this, 'normalize'], $args);
        $this->setCommandLast($sql, $param);
        
        $pdo = $this->getManager();
        
        if (count($param)) {
            $stm = $pdo->prepare($sql);
            $res = $stm->execute($param);
        } else {
            $res = $pdo->exec($sql);
        }
        return $res;
    }
    
    public function query($cmd)
    {
        $args = func_get_args();
        list($sql, $param) = call_user_func_array([$this, 'normalize'], $args);
        $this->setCommandLast($sql, $param);
        if ($this->getParam('no-exec')) {
            return null;
        }
        $pdo = $this->getManager();
        try {
            if (count($param)) {
                $res = $pdo->prepare($sql);
                $res->execute($param);
            } else {
                $res = $pdo->query($sql);
            }
        } catch (\Exception $e) {
            throw $e;
        }
        $pager = $this->getPager();
        return new DataSet($res, null, Util\arrayGet($pager, 'from', 0), Util\arrayGet($pager, 'count', 0));
    }
    
    
    protected function getPdo()
    {
        $this->connect();
        return $this->pdo;
    }
    
    public function setSchema($name)
    {
        $this->schema = $name;
    }
    public function getSchema()
    {
        return $this->schema;
    }

    public function connect()
    {
        if (!$this->pdo) {
            $this->pdo = $this->newPDO();
        }
    }

    public function delete($resource, $pkey, $observer = null, $forceDelete = false)
    {
        $this->connect();
        return $this->getLib()->getStrategyDelete($resource, $pkey)->exec($observer, $forceDelete);
    }

    public function insert($resource, $data, $observer = null)
    {
        $this->connect();
        return $this->getLib()->getStrategyInsert($resource, $data)->exec($observer);
    }

    public function update($resource, $data, $pkey, $observer = null)
    {
        $this->connect();
        return $this->getLib()->getStrategyUpdate($resource, $data, $pkey)->exec($observer);
    }

    public function align($info, $observer = null)
    {
        $this->connect();
        if ($name = Util\arrayGet($info, 'name') and $check = $this->check($name)) {
            $strategy = $this->getLib()->getStrategyUpgrade($info, $check);
            $event = 'upgrade';
        } else {
            $strategy = $this->getLib()->getStrategyCreate($info);
            $event = 'create';
        }
        // forza la rilettura della struttura in cache
        $ret = $strategy->exec();
        if ($observer and $ret) {
            if(is_callable($observer)){
                call_user_func_array($observer, [$event, $this]);
            } else {
                $observer->trigger($event, [$this]);
            }
        }
        if ($name) {
            $this->check($name, 1);
        }
        return $ret;
    }
    
    public function select($table, $pkey, $field = '', $order='', $debug = false)
    {
        $whr = [];
        $info = $this->getLib()->getDataStruct($table);
        if (!is_array($pkey)) {
            $pkey = ['id' => $pkey];
        }
        $data = [];
        if (Util\nvl($field, '*') == '*') {
            $field = array_keys(Util\arrayGet($info, 'fields'));
        }
        $fieldList = [];
        foreach(Util\asArray($field, ',') as $f) {
            $type = $this->getLib()->getDataType(Util\arrayGet($info, ['fields', $f, 'type']), []);
            $fieldList[] = $type->getSelectName($f);
        }
        foreach ($pkey as $p=>$v) {
            if ($v === null) {
                $whr[] = $p.' IS NULL';
            } else {
                $type = $this->getLib()->getDataType(Util\arrayGet($info, ['fields', $p, 'type']), []);
                $conf = $type->serializeData($p, $v);
                if ($conf[1] === null) {
                    $whr[] = $conf[0].' IS NULL';
                } else {
                    $whr[] = $conf[0].'='.$conf[1];
                }
                if ($conf[2]) {
                    $data[$conf[2]] = $conf[3];
                }
            }
        }
        $sql = "select ".implode(', ', $fieldList)."\n from {$table}\n where ".implode("\n   and ", $whr);
        if ($order) $sql .= " order by ".$order;
        return $this->query($sql, $data);
    }
    public function convert($fields, $data)
    {
        if ($data) {
            foreach($fields as $name => $conf) {
                if (array_key_exists($name, $data?:[])) {
                    switch ($conf['type']) {
                        case 'uuid':
                        case 'binary':
                            $data[$name] = strtoupper(bin2hex($data[$name]));
                    }
                }
            }
        }
        return $data;
    }
    public function quote($str, $wrap = false) 
    {
        $str = $this->getPdo()->quote($str);
        if ($wrap) {
            $str = trim($str, "'");
        }
        return $str;
    }
    public function setPager($from, $count)
    {
        if ($count) {
            $from = floor($from / $count) * $count;
        }
        $this->pager = ['from' => $from, 'count'=>$count];
    }
    public function getPager()
    {
        return $this->pager;
    }
    
}
