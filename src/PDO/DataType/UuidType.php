<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\DataType;

use Spinit\Lib\DataSource\DataType;
use Spinit\Util;

/**
 * Description of UuidType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class UuidType extends DataType
{
    public function make($nameField, $conf)
    {
        $size = Util\arrayGet($conf, 'size', '16');
        return $nameField.' binary('.$size.')';
    }
    
    public function serializeData($name, $value, $prefix = '')
    {
        $field = $prefix.$name;
        if ($value == '') {
            return [$name, null, '', ''];
        } else if (is_array($value)) {
            
            $listValue = [];
            $listField = [];
            foreach($value as $k=>$v) {
                $listValue []= 'unhex(:'.$field.'_'.$k.')';
                $listField []= $field.'_'.$k;
            }
            return [$name, $listValue, $listField, $value];
        }
        return [$name, 'unhex(:'.$field.')', $field, $value];
    }
    
    public function getSelectName($field) {
        return 'hex('.$field.') as '.$field;
    }
}
