<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\DataType;

use Spinit\Lib\DataSource\DataType;
use Spinit\Util;

/**
 * Description of UuidType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class JsonType extends DataType
{
    public function make($nameField, $conf)
    {
        return $nameField.' text';
    }
}
