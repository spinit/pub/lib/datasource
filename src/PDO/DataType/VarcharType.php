<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\DataType;

use Spinit\Lib\DataSource\DataType;
use Spinit\Util;

/**
 * Description of UuidType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class VarcharType extends DataType
{
    public function make($nameField, $conf) 
    {
        $conf['size'] = Util\nvl(Util\arrayGet($conf, 'size', Util\getenv('DEFAULT_VARCHAR_SIZE')), '50');
        return parent::make($nameField, $conf);
    }
}
