<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Manager\Mysql;

use Spinit\Lib\DataSource\PDO\DataLib as DataLibPdo;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;
use Spinit\Lib\DataSource\PDO\AdapterPDO;
/**
 * Description of DataLib
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataLib extends DataLibPdo
{

    public function getDataStruct($table)
    {
        if (strpos($table, '.') === false) {
            $par = ['table_schema'=>$this->getManager()->getSchema(), 'table_name'=>trim(trim($table,"'"),'`')];
            $table = '`'.$this->getManager()->getSchema().'`.`'.$table.'`';
        } else {
            $part = explode('.', $table);
            $par = ['table_schema'=>trim(trim($part[0],"'"),'`'), 'table_name'=>trim(trim($part[1],"'"),'`')];
            //throw new \Exception('bho');
        }
        $rec = $this->getManager()->query("
            select table_name 
            from information_schema.tables 
            where table_schema = :table_schema 
              and table_name = :table_name
              and table_type like '%TABLE%'", $par)->first();
        if (!$rec) {
            throw new NotFoundException('Risorsa non trovata : '.$table);
        }
        $info = ['name' => $rec['table_name'], 'fields'=>[], 'pkey'=>[], 'index'=>[]];
        
        $list = $this->getManager()->query("DESCRIBE {$table}");
        foreach($list as $rec) {
            $type = Util\arrayGet($rec, 'Type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?( (\w+))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $type = $LVar[1][0];
            $size = $LVar[3][0];
            switch($type) {
                case 'binary':
                    if ($size == max(intval(AdapterPDO::$UUID_SIZE), 8)) {
                        list($type, $size) = ['uuid', ''];
                    }
                    break;
                case 'bigint':
                    if ($LVar[5][0] == 'unsigned' and Util\arrayGet($rec, 'Extra') == 'auto_increment') {
                        list($type, $size) = ['increment', ''];
                    }
                    break;
            }
            
            $info['fields'][$rec['Field']] = [
                'type' => $type,
                'size' => $size,
                'unsigned'  => ($LVar[5][0]=='unsigned'),
                'autoinc'   => ($rec['Extra'] == 'auto_increment'),
                'ispkey'    => (Util\arrayGet($rec, 'Key') == 'PRI' ? true : false),
                'notnull'   => ($rec['Null'] != 'YES'),
                'default'   => $rec['Default']
            ];
            
            if (Util\arrayGet($rec, 'Key')=='PRI') {
                $info['pkey'][] = $rec['Field'];
            }
        }
        /*
        foreach($this->getManager()->load("SHOW INDEX FROM {$table}") as $rec) {
            try {
                $index = $ds->getIndex($rec['Key_name']);
            } catch (NotFoundException $ex) {
                $index = $ds->addIndex(new Index($rec['Key_name'], $rec['Index_type']));
            }
            $index->addField($rec['Column_name']);
        }
         * 
         */
        return $info;
    }

}
