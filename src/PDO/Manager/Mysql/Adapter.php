<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Manager\Mysql;

use Spinit\Lib\DataSource\PDO\AdapterPDO;
use Spinit\Util;

/**
 * Description of DataSource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Adapter extends AdapterPDO
{
    protected function makeLib()
    {
        return new DataLib($this);
    }
    public function getConnectionString()
    {
        return "{$this->getInfo(0)}:{$this->getInfo(1)}:{$this->getInfo(2)}:{$this->getInfo(3)}:{$this->getInfo(4)}";
    }
    protected function newPDO()
    {
        $charsetInfo = explode(',', $this->getParam('mysql.charset'));
        $charset = array_shift($charsetInfo);
        if (!$charset) {
            $charset = 'utf8mb4';
            $collate = 'utf8mb4_unicode_ci';
        } else {
            $collate = array_shift($charsetInfo);
        }
        $opt = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".$charset);
        $arHost = explode('#', $this->getInfo(1));
        list($host, $port) = [array_shift($arHost), Util\nvl(array_shift($arHost),'3306')];
        $nameDB = $this->getInfo(2);
        $username = $this->getInfo(3);
        $password = $this->getInfo(4);
        $create = $this->getInfo(5);
        $engine = mb_strtoupper($this->getInfo(6));
        
        $strcn = "mysql:host={$host};port={$port};";
        try {
            $pdo = new \PDO($strcn, $username, $password, $opt);
        } catch (\Exception $e) {
            throw $e;
        }
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        $pdo->info = ['charset' => 'CHARACTER SET '.$charset.($collate ? ' COLLATE '.$collate : ''), 'engine'=>$engine];
        try {
            $pdo->exec("USE `{$nameDB}`");
        } catch (\Exception $e) {
            if (!$create) {
                throw $e;
            }
            $pdo->exec("CREATE DATABASE `{$nameDB}` ".$pdo->info['charset']);
            $this->trigger('createDB.mysql');
        }
        $pdo->exec("USE `{$nameDB}`");
        $this->setSchema($pdo->schema = $nameDB);
        return $pdo;
    }

    public function setSchema($name)
    {
        $manager = $this->getManager(true);
        if ($manager) {
            $manager->exec("USE `{$name}`");
            $manager->schema = $name;
        }
        parent::setSchema($name);
    }
    
    public function getType() {
        return 'mysql';
    }
    
    public function getTypeList() {
        return ['mysql', 'mysqli', 'sql'];
    }
    public function getName() {
        return $this->getInfo(2);
    }
}
