<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Manager\Sqlite;

use Spinit\Lib\DataSource\PDO\AdapterPDO;

/**
 * Description of DataSource
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Adapter extends AdapterPDO
{
    
    protected function newPDO()
    {
        $pdo = new \PDO($this->getConnectionString());
        // Set errormode to exceptions
        $pdo->setAttribute(\PDO::ATTR_ERRMODE,
                           \PDO::ERRMODE_EXCEPTION);
        $pdo->sqliteCreateFunction('md5', function ($str) {return md5($str); }, 1);
        $pdo->sqliteCreateFunction('unhex', function ($str) { return $str; }, 1);
        $pdo->sqliteCreateFunction('hex', function ($str) { return $str; }, 1);
        
        $pdo->info = ['charset' => '', 'engine'=>$this->getInfo(2)];
        return $pdo;
    }

    protected function makeLib()
    {
        return new DataLib($this);
    }
    public function getType() {
        return 'sqlite';
    }
    
    public function getTypeList() {
        return ['sqlite'];
    }

    public function getConnectionString() {
        $fname  = $this->getInfo(1);
        $engine = $this->getInfo(2);
        
        $strcn = "{$this->getInfo(0)}:";
        if ($fname) {
            $strcn .= $fname;
        } else {
            $strcn .= ":{$engine}:";
        }
        return $strcn;
    }
}
