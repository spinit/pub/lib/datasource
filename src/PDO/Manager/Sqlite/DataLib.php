<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO\Manager\Sqlite;

use Spinit\Lib\DataSource\PDO\DataLib as DataLibPdo;
use Spinit\Util;
/**
 * Description of DataLib
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataLib extends DataLibPdo
{

    public function getDataStruct($table)
    {
        $cmd = "select name from sqlite_master where type='table' and name = {{table}}";
        $rec = $this->getManager()->query($cmd, ['table'=>$table])->first();
        if (!$rec) {
            return false;
        }
        $info = ['name' => $rec['name'], 'fields'=>[], 'pkey'=>[], 'index'=>[]];
        
        foreach($this->getManager()->query("PRAGMA table_info('{$table}')") as $rec) {
            $type = Util\arrayGet($rec, 'type', 'text');
            @preg_match_all("/(\w+)(\((\w+)\))?/", $type, $LVar, PREG_PATTERN_ORDER);
            $type = $LVar[1][0];
            $size = $LVar[3][0];
            $default = $rec['dflt_value'];
            if (!defined('UUID_SIZE')) define('UUID_SIZE', 16);
            switch($type) {
                case 'binary':
                    if ($size == UUID_SIZE) {
                        list($type, $size) = ['uuid', ''];
                    }
                    break;
                case 'bigint':
                    if (array_key_exists(5, $LVar) and $LVar[5][0] == 'unsigned' and Util\arrayGet($rec, 'Extra') == 'auto_increment') {
                        list($type, $size) = ['increment', ''];
                    }
                    break;
                case 'varchar':
                    if ($default == 'NULL') {
                        $default = null;
                    } else if (substr($default, 0, 1) == "'") {
                        $default = substr(str_replace("''", "'", $default), 1, -1);
                    }
                    break;
            }
            $info['fields'][$rec['name']] = [
                'type' => $type,
                'size' => $size,
                'unsigned'  => (array_key_exists(5, $LVar) and $LVar[5][0]=='unsigned'),
                'autoinc'   => (Util\arrayGet($rec, 'Extra') == 'auto_increment'),
                'ispkey'    => (Util\arrayGet($rec, 'key') == '1' ? true : false),
                'notnull'   => ($rec['notnull']=='1'?'1':''),
                'default'   => $default,
                'incval'    => ''
            ];
            if (Util\arrayGet($rec, 'pk')=='1') {
                $info['pkey'][] = $rec['name'];
            }
        }
        /*
        foreach($this->getManager()->load("SHOW INDEX FROM {$table}") as $rec) {
            try {
                $index = $ds->getIndex($rec['Key_name']);
            } catch (NotFoundException $ex) {
                $index = $ds->addIndex(new Index($rec['Key_name'], $rec['Index_type']));
            }
            $index->addField($rec['Column_name']);
        }
         * 
         */
        return $info;
    }
    public function getDataType($type, $conf)
    {
        switch($type) {
            case 'uuid':
                $conf['size'] = '64';
                return parent::getDataType('varchar', $conf);
        }
        return parent::getDataType($type, $conf);
    }
}
