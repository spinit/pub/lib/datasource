<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\PDO;

use Spinit\Lib\DataSource\PDO\Strategy\DaImplementare;
use Spinit\Lib\DataSource\PDO\Strategy\TableCreate;
use Spinit\Lib\DataSource\PDO\Strategy\TableUpgrade;
use Spinit\Lib\DataSource\PDO\Strategy\TableTrace;
use Spinit\Lib\DataSource\PDO\Strategy\CommandInsert;
use Spinit\Lib\DataSource\PDO\Strategy\CommandUpdate;
use Spinit\Lib\DataSource\PDO\Strategy\CommandDelete;
use Spinit\Util;
use Spinit\Lib\DataSource\DataLib as DataLibBase;
use Spinit\Util\Error\NotFoundException;
/**
 * Description of Lib
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class DataLib extends DataLibBase
{
    public function getStrategyCreate($info)
    {
        $strategy = new TableCreate($this->getManager(), $info);
        if (Util\arrayGet($info, 'trace')) {
            $strategy = new TableTrace($this->getManager(), $info, $strategy);
        }
        return $strategy;
    }
    public function getStrategyUpgrade($info, $check)
    {
        $strategy = new TableUpgrade($this->getManager(), $info, $check);
        if (Util\arrayGet($info, 'trace')) {
            $strategy = new TableTrace($this->getManager(), $info, $strategy);
        }
        return $strategy;
    }
    public function getStrategyInsert($resource, $data)
    {
        return new CommandInsert($this->getManager(), $resource, $data);
    }
    public function getStrategyUpdate($resource, $data, $pkey)
    {
        return new CommandUpdate($this->getManager(), $resource, $data, $pkey);
    }
    public function getStrategyDelete($resource, $pkey)
    {
        return new CommandDelete($this->getManager(), $resource, $pkey);
    }
    
    public function makeField($name, $conf)
    {
        $type = $this->getDataType(Util\arrayGetAssert($conf, 'type'), $conf);
        return $type->make($name, $conf);
    }
    public function getDataType($type, $conf)
    {
        switch($type) {
            case 'date':
            case 'datetime':
                return new DataType\DateTimeType($type);
            case 'binary':
            case 'uuid':
                return new DataType\UuidType($type);
            case 'varchar':
                return new DataType\VarcharType($type);
            case 'money':
                return new DataType\MoneyType($type);
            case 'json':
                return new DataType\JsonType($type);
        }
        return parent::getDataType($type, $conf);
    }
    
    public function makePkey($pkey)
    {
        return 'PRIMARY KEY ('.implode(',', Util\asArray($pkey, ',')).')';
    }
    
    public function unhex($str) {
        if (!$str) {
            return null;
        }
        return "unhex('{$str}')";
    }
    
    public function addColumn($table, $name, $conf)
    {
        return 'ALTER TABLE '.$table.' ADD '.$this->makeField($name, $conf);
        
    }
    
    public function getQuery($query) {
        if (is_string($query) and array_key_exists('query', $query)) {
            $query = $query['query'];
        }
        if (is_string($query)) {
            return $query;
        }
        foreach ($this->getManager()->getTypeList() as $proto) {
            if (array_key_exists($proto, $query)) {
                return $query[$proto];
            }
        }
        throw new NotFoundException('query non trovata per ['.$this->getManager()->getType().']');
    }
}
