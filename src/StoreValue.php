<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of StoreValue
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class StoreValue
{
    private $func;
    private $values;
    
    public function __construct($func)
    {
        $args = func_get_args();
        $this->func = array_shift($args);
        $this->values = $args;
        // può capitare che venga istanziato su uno storeValue ... quindi occorre
        // ricercare il valore originario
        while ($this->getValue() instanceof self) {
            $this->values = [$this->getValue()->getValue()];
        }
    }
    
    public function getFunction()
    {
        return $this->func;
    }
    
    public function getValue()
    {
        if (!count($this->values)) {
            return "";
            //throw new NotFoundException();
        }
        return $this->values[0] ?: "";
    }

    public function __toString() {
        return $this->getValue();
    }
}
