<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use Spinit\Util;
use Spinit\Lib\DataSource\StoreValue;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of DataType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataType
{
    private $nameType;
    
    public function __construct($nameType)
    {
        $this->nameType = $nameType;
    }
    public function make($nameField, $conf)
    {
        $str = ' '.$nameField. ' '.$this->nameType;
        if ($size = Util\arrayGet($conf, 'size')) {
            $str .= '('.$size.')';
        }
        return $str;
    }
    
    public function serializeData($name, $value, $prefix = '')
    {
        $fnc = '';
        try {
            if ($value instanceof StoreValue) {
                $fnc = $value->getFunction();
                $value = $value->getValue();
            }
            if (is_array($value)) {
                $value = json_encode($value);
            }
            return[$name, 
                $value === null ? null : ($fnc?$fnc.'(:'.$prefix.$name.')' : ':'.$prefix.$name), 
                $value === null ? '' : $prefix.$name, 
                $value
            ];
        } catch (NotFoundException $e) {
            return[$name, $fnc.'()', null, null];
        }
    }
    
    public function getSelectName($field)
    {
        return $field;
    }
}
