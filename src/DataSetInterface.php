<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

/**
 * Description of DataSet
 *
 * @author ermanno
 */
interface DataSetInterface extends \Iterator {
    function count();
    function first($origin = false);
    function next();
    function current($origin = false);
    function close();
    function isOpen();
}
