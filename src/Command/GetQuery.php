<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\Command;

/**
 * Description of GetQuery
 *
 * @author ermanno
 */
class GetQuery {
    
    private $DS;
    
    public function __construct($adapter) {
        $this->DS = $adapter;
    }
    
    public function exec() {
        $args = func_get_args();
        $query = $args[0];
        if (is_array($query) and array_key_exists('query', $query)) {
            $query = $query['query'];
        }
        $cmd = $query;
        if (is_array($query)) {
            foreach($this->DS->getTypeList() as $type) {
                $cmd = \Spinit\Util\arrayGet($query, $type);
                if ($cmd) break;
            }
        }
        $args[0] = $cmd;
        return $args;
    }
}
