<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use Spinit\Lib\DataSource\DataSetInterface;
use Spinit\Util;

/**
 * Description of DataSetArray
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DataSetArray extends \ArrayIterator implements DataSetInterface
{
    private $meta = false;
    
    public function close() {
        
    }

    public function setMetadata($meta)
    {
        $this->meta = $meta;
    }
    public function getMetadata($type = '')
    {
        if ($this->meta !== false) {
            return $this->meta;
        }
        if ($row = $this->current()) {
            foreach ($row as $k => $v) {
                $meta[$k] = ['name'=>$k, 'label'=>$k];
            }
            return $meta;
        }
        return null;
    }

    public function isOpen() {
        
    }

    public function current($origin = false) {
        return parent::current();
    }
    public function first($origin = false)
    {
        return Util\arrayGet($this, 0);
    }
    public function position() {
        
    }

    public function rowCount()
    {
        return $this->count();
    }

    public function getPager()
    {
        return ['from'=>0, 'count'=>$this->count(), 'total'=>$this->count()];
    }

    public function getCount() {
        return $this->count();
    }

    public function getFrom() {
        return 0;
    }

}
