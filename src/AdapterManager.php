<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use Spinit\Util;

/**
 * Description of AdapterManager
 *
 * @author <ermanno.astolfi@spinit.it>
 */
class AdapterManager {
    static private $list = [];
    static private $adapters = [];
    
    static public function addAdapter($protocol, $classPath) {
        foreach(Util\asArray($protocol, ',') as $proto) {
            self::$list[$proto] = $classPath;
        }
    }
    
    static public function getAdapter($connection) {
        $args = $connection;
        if (!is_array($args)) $args = explode(':', $args);
        $firma = trim(implode(':', $args),':');

        array_unshift($args, Util\arrayGetAssert(self::$list, $args[0]));
        return Util\arrayGet(self::$adapters, $firma, function() use ($firma, $args) {
            $adapter = call_user_func_array("\\Spinit\\Util\\getInstance", $args);
            return self::$adapters[$firma] = $adapter;
        });
    }
}
