<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use Spinit\Lib\DataSource\DataSourceInterface;
use Spinit\Lib\DataSource\AdapterManager;

use Webmozart\Assert\Assert;
use Spinit\Util;

/**
 * Description of DataSource
 *
 * @author ermanno
 */
class DataSource implements DataSourceInterface {
    
    private $adapter = null;
    private $counter;
    
    public function __construct($conString = '') {
        if ($conString) {
            $this->connect($conString);
        }
    }
    public function getConnectionString() {
        if (!$this->adapter) return '';
        return $this->adapter->getConnectionString();
    }
    public function connect($conString) {
        $this->adapter = AdapterManager::getAdapter($conString);
        return $this;
    }
    public function reconnect()
    {
        if($this->adapter) $this->adapter->reconnect();
    }
    public function getAdapter($checkNull = true) {
        if ($checkNull) {
            Assert::notNull($this->adapter, 'DataBase non connesso');
        }
        return $this->adapter;
    }
    public function exec($cmd) {
        $args = func_get_args();
        $xcmd = new Command\GetQuery($this->getAdapter(true));
        $query = call_user_func_array([$xcmd, 'exec'], $args);
        Assert::notEmpty($query[0]);
        // la sostituzione dei parametri deve essere effettuata dall'adapter
        return call_user_func_array([$this->getAdapter(),'exec'], $query);
    }

    public function query($cmd) {
        $args = func_get_args();
        $xcmd = new Command\GetQuery($this->getAdapter(true));
        $query = call_user_func_array([$xcmd, 'exec'], $args);
        Assert::notEmpty($query[0], 'Query non impostata');
        // la sostituzione dei parametri deve essere effettuata dall'adapter
        return call_user_func_array([$this->getAdapter(),'query'], $query);
    }
    public function getCommand($query) {
        $args = func_get_args();
        $xcmd = new Command\GetQuery($this->getAdapter(true));
        $list = call_user_func_array(
                        [$xcmd, 'exec'], 
                        $args
                    );
        return $list;
    }
    public function getCommandLast() {
        return $this->getAdapter(true)->getCommandLast();
    }
    
    public function insert($resource, $data) {
        return $this->getAdapter(true)->insert($resource, $data);
    }

    public function update($resource, $data, $key) {
        return $this->getAdapter(true)->update($resource, $data, $key);
    }
    
    public function delete($resource, $key, $observer = null, $forceDelete = false) {
        return $this->getAdapter(true)->delete($resource, $key, $observer, $forceDelete);
    }

    public function select($resource, $key, $fields = '', $order = '') {
        $rec = $this->getAdapter(true)->select($resource, $key, $fields, $order);
        return $rec;
    }

    public function align($resourceStruct, $observer = null) {
        $this->getAdapter(true)->align($resourceStruct, $observer);
        return $this;
    }

    public function check($resourceName) {
        return $this->getAdapter(true)->check($resourceName);
    }

    public function drop($resourceName) {
        $this->getAdapter(true)->drop($resourceName);
        return $this;
    }

    public function truncate($resourceName) {
        $this->getAdapter(true)->truncate($resourceName);
        return $this;
    }

    public function getPkeyDefault() {
        return $this->getAdapter(true)->getPkeyDefault();
    }
    public function getCounter() {
        return $this->counter;
    }
    public function setCounter($counter) {
        $this->counter = $counter;
        return $this;
    }
}
