<?php
namespace Spinit\Lib\DataSource\Type;

/**
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface DataSetInterface extends \Iterator
{
    
    public function isOpen();
    
    /**
     * Chiude la sorgente dati
     */
    public function close();
    
    /**
     * Restiuisce la posizione del record corrente
     */
    public function position();
    
    /**
     * Restituisce le informazioni associate al dataset
     */
    public function getMetadata($type = '');
    
    public function rowCount();
    public function getPager();
    public function getFrom();
    public function getCount();
}
