<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\Type;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface DataSourceInterface
{
    function getManager();
    function getInfo();
    function getLib();
    function connect();
    
    function exec($cmd);
    function load($cmd);
    function find($resource, $field, $pkey);
    
    function check($resource, $nocache = false);
    function align($info, $observer = null);
    function insert($resource, $data, $observer = null);
    function update($resource, $data, $pkey, $observer = null);
    function delete($resource, $pkey, $observer = null);
    
    function setPager($from, $count);
    function getPager();
}
