<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use Spinit\Util;

/**
 * Description of GetDataSourceTrait
 *
 * @author ermanno
 */
trait GetDataSourceTrait {
    
    private $datasource = [];
    
    public function setDataSource($DataSource, $name = '') {
        if (is_string($DataSource)) {
            $DataSource = $this->makeDataSource($DataSource);
        }
        $this->datasource[$name] = $DataSource;
        return $this;
    }
    
    public function getDataSource($name = '') {
        return Util\arrayGetAssert($this->datasource, $name);
    }
    
    public function makeDataSource($strincConnection) {
        return new DataSource($strincConnection);
    }
    
    public function getDataSourceList() {
        return $this->datasource;
    }
}
