<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use Spinit\Util;
use Webmozart\Assert\Assert;

use function Spinit\Dev\AppRouter\debug;

/**
 * Description of DataLib
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class DataLib 
{

    private $normalizeNull = true;
    private $manager;
    public $debug;
    
    public function __construct(AdapterInterface $ds)
    {
        $this->setManager($ds);
    }
    
    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }
    
    public function getManager()
    {
        return $this->manager;
    }
    
    public abstract function getDataStruct($resource);
    public abstract function unhex($str);
    
    public function notNormalizeNull()
    {
        $this->normalizeNull = false;
    }
    
    public function getSql($query)
    {
        if (is_string($query)) {
            return $query;
        }
        if (isset($query['query'])) {
            $query = $query['query'];
        }
        $sql = '';
        // query specifica per il tipo database utilizzato
        foreach($this->getManager()->getTypeList() as $type) {
            $sql  = Util\arrayGet($query, $type);
            if ($sql) {
                break;
            }
        }
        if (!$sql) {
            // query generica
            $sql  = Util\arrayGet($query, 'sql');
        }
        Assert::notEmpty($sql, "Query not found");
        return $sql;
    }    
    private function alignVar($sql, &$vars)
    {
        $vList = Util\arrayGet($sql, ['var'], []);
        if (array_key_exists('name', $vList)) {
            $vList = [$vList];
        }
        foreach ($vList as $v) {
            $vars[$v['name']] = $v['value'];
            if ($f = Util\arrayGet($v, 'apply')) {
                $vars[$v['name']] = call_user_func($f, $v['value']);
            }
        }
    }
    /**
     * La normalizzazione della query avviene in due passi
     * - viene chiesto al database se occorre modificare lo script individuato (es. effettuare delle sostituzioni
     *   con dei placeolder)
     * - vengono sostituiti nella query stessa i parametri che sono presenti nella forma {{@?parametro}}
     * @param type $sqlraw
     * @param type $params
     * @return type
     */
    public function normalize($sqlraw, $params=array(), $args = array(), $info = array(), $vars = array())
    {
        if (is_array($sqlraw)) {
            debug(func_get_args());
        }
        foreach(['params', 'args', 'info', 'vars'] as $dd) {
            $$dd = $$dd ?: [];
            if (!is_array($$dd)) {
                $$dd = ['id' => $$dd];
            }
        }
        $debug = false;
        if (is_array($sqlraw) and array_key_exists('debug', $sqlraw)) {
            $debug = $sqlraw['debug'];
        }
        $this->alignVar($sqlraw, $vars);
        $sqlraw = $this->getSql($sqlraw);
        // sostituzione nome database
        $sql = $this->getManager()->trigger('normalizeQuery', $sqlraw);
        if (!$sql or !is_string($sql)) {
            $sql = $sqlraw;
        }
        list($sql, $params) = Util\normalize([$sql, function() {
            $args = func_get_args();
            return call_user_func_array([$this, 'normalizeValue'], $args);
        }, $debug], $params, $args, $info, $vars);
        
        if ($this->normalizeNull) {
            // sostituzione valorizzazioni NULL
            $sql = str_replace(array("!= NULL", "<> NULL"), array(' IS NOT NULL', ' IS NOT NULL'), $sql);
            $sql = str_replace(array("= NULL"), array(' IS NULL'), $sql);
        } else {

            // una volta usato ... si reimposta al valore naturale
            $this->normalizeNull = true;
        }
        // se nei parametri ci sono rimasti elementi array ... vengono tolti
        foreach($params ?: [] as $key => $value) {
            if (is_array($value)) {
                $params[$key] = json_encode($params[$key]);
                //unset($params[$key]);
            }
        }
        if ($debug) {
            var_dump($sql, $params); exit;
        }
        // viene restituita la query pronta per l'esecuzione con l'insieme di parametri che sono affidati al driver del DB
        return array($sql, $params);
    }
    
    protected function normalizeValue($field, $params, $matches, $k, $sql)
    {
        $unset = [];
        // non vuole gli apici finali?
        $strip = ($field[0] == ':');
        $field = $strip ? substr($field, 1) : $field;
        // è un esadecimale?
        $at = ($field[0] == '@');
        $field = $at ? substr($field, 1) : $field;
        // è una lista di valori?
        $lst = ($field[0] == '+');
        $field = $lst ? substr($field, 1) : $field;
        // non vuole la formattazione della stringa?
        $plain = ($field[0] == '*');
        if ($plain) {
            $field =  substr($field, 1);
        }
        foreach(explode('.', $field) as $ff) {
            if (array_key_exists($ff, $params)) {
                if ($params[$ff] === null) {
                    if ($strip) {
                        $value = '';
                    } else {
                        $value = null;
                    }
                    break;
                }
                if (is_array($params[$ff])) {
                    $value = $params[$ff];
                    $params = &$params[$ff];
                    continue;
                } else if ($params[$ff] instanceof Util\DictionaryBase) {
                    $params = $params[$ff]->asArray();
                    $value = $params;
                    continue;
                }
                if ($at) {
                    $value = $this->unhex($params[$ff]);
                } else {
                    $value = $plain ? $params[$ff] : $this->getManager()->quote($params[$ff]);
                    if ($strip) {
                        $value = substr($value, 1, -1);
                    }
                }
                if (!in_array($ff, $unset)) {
                    $unset[] = $ff;
                }
            } else {
                // se un valore non è definito allora viene impostato come stringa vuota
                // per metterlo a null occorre impostarlo con il valore [null]
                $value = $strip ? "":"''";
                $value = $at ? null : $value;
            }
            break;
        }
        if ($value === null) {
            if ($plain) {
                $value = '';
            } else {
                if ($lst) {
                    $value = "''";
                } else {
                    $value = 'NULL';
                }
            }
        } else if (is_array($value)) {
            if ($lst) {
                $ll = [];
                foreach($value as $val) {
                    if (is_array($val)) {
                        throw new \Exception('Tipo dato errato : '.json_encode($value, JSON_UNESCAPED_UNICODE));
                    }
                    if ($at) {
                        $ll []= $this->unhex($val);
                    } else {
                        $ll []= $plain ? $val :$this->getManager()->quote($val);
                    }
                }
                if (count($ll)) {
                    $value = implode(', ', $ll);
                } else {
                    $value = $plain ? "" : "''";
                }
            } else {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            }
        }
        $sql = str_replace($matches[$k], $value, $sql);
        return [$sql, $unset];
    }

    public function getDataType($type, $conf)
    {
        return new DataType($type);
    }
    
    
}
