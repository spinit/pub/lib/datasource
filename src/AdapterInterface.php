<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

/**
 *
 * @author ermanno
 */
interface AdapterInterface {
    public function getType();
    public function getTypeList();
    public function getPkeyDefault();
    function check($resourceName);
    function normalize();
    function getConnectionString();
}
