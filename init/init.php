<?php
use Spinit\Lib\DataSource\AdapterManager;

AdapterManager::addAdapter("mysql, mysqli, mariadb", "Spinit:Lib:DataSource:PDO:Manager:Mysql:Adapter");
AdapterManager::addAdapter("sqlite", "Spinit:Lib:DataSource:PDO:Manager:Sqlite:Adapter");
