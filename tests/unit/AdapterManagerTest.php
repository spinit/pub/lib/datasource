<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource;

use PHPUnit\Framework\TestCase;

/**
 * Description of AdapterManagerTest
 *
 * @author ermanno
 */
class AdapterManagerTest extends TestCase {
    
    public function testAdapter() {
        AdapterManager::addAdapter('uno, due', "Spinit:Lib:DataSource:Tests:TestAdapter");
        $adapter = AdapterManager::getAdapter("uno:prova");
        $this->assertTrue($adapter instanceof Tests\TestAdapter);
        $this->assertEquals(['uno', 'prova'], $adapter->getArgs());
        $this->assertTrue($adapter === AdapterManager::getAdapter("uno:prova"));
        $this->assertTrue($adapter !== AdapterManager::getAdapter("uno:prova2"));
    }
}
