<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\DataSource\Tests;

/**
 * Description of TestAdapter
 *
 * @author ermanno
 */
class TestAdapter {
    
    private $args = null;
    
    public function __construct() {
        $this->args = func_get_args();
    }
    
    public function getArgs()
    {
        return $this->args;
    }
}
